// Namespace Configuration Values
export const ED4E = {};

// which tier belongs a given circle to?
// the index equals the circle, therefore index 0 is undefined
ED4E.circleToTier = [
  undefined,
  'novice', // 1
  'novice', // 2
  'novice', // 3
  'novice', // 4
  'journeyman', // 5
  'journeyman', // 6
  'journeyman', // 7
  'journeyman', // 8
  'warden', // 9
  'warden', // 10
  'warden', // 11
  'warden', // 12
  'master', // 13
  'master', // 14
  'master', // 15
];

// cost of improving talent ranks
// the index equals the circle, as above, index 0 is 0 since it does not cost LP to learn a talent at rank 0
ED4E.talentCostTable = {
  novice: [0, 100, 200, 300, 500, 800, 1300, 2100, 3400, 5500, 8900, 14400, 23300, 37700, 61000, 98700],
  journeyman: [0, 200, 300, 500, 800, 1300, 2100, 3400, 5500, 8900, 14400, 23300, 37700, 61000, 98700, 159700],
  warden: [0, 300, 500, 800, 1300, 2100, 3400, 5500, 8900, 14400, 23300, 37700, 61000, 98700, 159700, 258400],
  master: [0, 500, 800, 1300, 2100, 3400, 5500, 8900, 14400, 23300, 37700, 61000, 98700, 159700, 258400, 418100],
};

// cost of improving skill ranks
// indexing analogous to talentCostTable
ED4E.skillCostTable = {
  novice: [0, 200, 300, 500, 800, 1300, 2100, 3400, 5500, 8900, 14400, 23300, 37700, 61000, 98700, 159700],
  journeyman: [0, 300, 500, 800, 1300, 2100, 3400, 5500, 8900, 14400, 23300, 37700, 61000, 98700, 159700, 258400],
  warden: [0, 500, 800, 1300, 2100, 3400, 5500, 8900, 14400, 23300, 37700, 61000, 98700, 159700, 258400, 418100],
};

// how long it takes (in weeks) to train to learn a skill to the given rank
// indexing analogous to above
ED4E.skillTrainingTime = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

// how long it takes (in weeks) to train to wait to improve a skill again
// indexing analogous to above
ED4E.skillWaitingTime = [0, 2, 3, 5, 8, 13, 21, 34, 55, 89, Infinity];

// The cost of a spell is equivalent to the cost of purchasing a Novice talent at a Rank equal to the Spell Circle.
// indexing as above
ED4E.spellLPcost = [0, 100, 200, 300, 500, 800, 1300, 2100, 3400, 5500, 8900, 14400, 23300, 37700, 61000, 98700];

// a mapping of abbreviations of attributes in the data model
ED4E.attributeAbbreviations = {
  DEX: 'dexterity',
  TOU: 'toughness',
  STR: 'strength',
  PER: 'perception',
  WIL: 'willpower',
  CHA: 'charisma',
};


  //#######################################################################################
  /** Translation helper */
  //#######################################################################################
  ED4E.typeTranslations = {
    attack: "earthdawn.a.attack",
    power: "earthdawn.p.power",
    skill: "earthdawn.s.skill",
    talent: "earthdawn.t.talent",
    spell: "earthdawn.s.spell",
    devotion: "earthdawn.d.devotion",
    damage: "earthdawn.d.damage"
  }
