import { karmaAllowed } from '../helpers/karma-allowed.js';
import { getTargetDifficulty } from '../helpers/combat-modifiers.js';
import { devotionAllowed } from '../helpers/devotion-allowed.js';
import { getOptionBox } from '../helpers/option-box.js';
import { readMods } from '../helpers/active-mods.js';
import { actionTestModifiers, defenseModifiers, damageTestModifiers, initiativeMod } from '../helpers/combat-modifiers.js';
import { rollDice } from '../helpers/roll-dice.js';

export async function rollPrep(actor, inputs) {
  console.log('[EARTHDAWN] Roll Prep Input', inputs);

  //#######################################################################################
  /** Constants */
  //#######################################################################################
  const weapon = inputs.weaponID ? actor.items.get(inputs.weaponID) : null;
  const attackTalentID = inputs.attackTalentID ? inputs.attackTalentID : weapon ? await actor.getAttack(weapon.system.weapontype) : null;
  const damageTalent = inputs.damageTalentID ? inputs.damageTalentID : null;
  const item = inputs.talentID ? actor.items.get(inputs.talentID) : attackTalentID ? actor.items.get(attackTalentID) : null;
  const att = inputs.attribute ? inputs.attribute : item ? item.system.attribute : weapon ? 'dexterityStep' : null;
  const modifier = inputs.modifier ? inputs.modifier : 0;
  const targets = inputs.targets ? inputs.targets : null;
  const ranks = inputs.ranks
    ? inputs.ranks
    : item
    ? item.system.finalranks
      ? item.system.finalranks
      : item.system.ranks
      ? item.system.ranks
      : 0
    : 0;
  const title = inputs.talentName
    ? inputs.talentName
    : item
    ? item.name
    : inputs.talent
    ? inputs.talent
    : game.i18n.localize(`earthdawn.${att.slice(0, 1)}.${att.slice(0, -4)}`);
  const type = item ? item.type : '';
  const rolltype = inputs.rolltype ? inputs.rolltype : inputs.type === 'attack' ? 'attack' : '';
  const karma = inputs.karmaNumber ? inputs.karmaNumber : 0;
  const damageBonus = inputs.damageBonus ? inputs.damageBonus : 0;
  const steps = inputs.steps ? inputs.steps : actor.type !== 'pc' ? inputs.finalstep : null;
  const devotion = inputs.devotionNumber ? inputs.devotionNumber : 0;
  const initiative = inputs.initiative ? inputs.initiative : false;
  const strain =
    (inputs.strain ? Number(inputs.strain) : 0) +
    (item != null ? Number(item.system.strain) : 0) +
    (actor.system.tactics.aggressive === true &&
    rolltype === 'attack' &&
    (weapon.system.weapontype === 'Melee' || weapon.system.weapontype === 'Unarmed' || actor.type === 'creature')
      ? 1
      : 0);
  const spellName = inputs.spellName ? inputs.spellName : '';
  const spellId = inputs.spellId ? inputs.spellId : '';

  const damageId = damageTalent ? damageTalent : weapon ? inputs.weaponID : null;

  const defenseTarget = inputs.defenseTarget ? inputs.defenseTarget : item ? item.system.defenseTarget : '';
  const defenseGroup = inputs.defenseGroup ? inputs.defenseGroup : item ? item.system.defenseGroup : '';

  const difficulty = inputs.difficulty
    ? inputs.difficulty
    : rolltype === 'attack'
    ? getTargetDifficulty('physicaldefense')
    : getTargetDifficulty(defenseTarget, defenseGroup);
  //const power = inputs.

  let wounds = actor.system.wounds;
  wounds = actor.woundCalc(wounds);

  let karmarequested;
  if (karma > 0) {
    karmarequested = 'true';
  } else {
    karmarequested = actor.system.usekarma;
  }

  //#######################################################################################
  /** Devotions requested */
  //#######################################################################################
  let devotionrequested;
  console.log('inputs.devotionNumber: ' + inputs.devotionNumber);
  console.log('devotionrequested: ' + devotionrequested);

  if (item) {
    if (item.system.devotionRequired === 'true' && item.type === 'devotion') {
      devotionrequested = 'true';
    } else {
      devotionrequested = 'false';
    }
  } else {
    if (!item) {
      devotionrequested = 'false';
    }
  }

  //#######################################################################################
  /** Devotions Die */
  //#######################################################################################
  let devotionDie;
  if (actor.system.devotion.max !== 0) {
    devotionDie = actor.system.devotionDie;
  } else if (actor.system.devotion.max === 0) {
    devotionDie = 'd4';
  }

  //#######################################################################################
  /** Base Steps */
  //#######################################################################################
  let basestep = actor.system[att] ? actor.system[att] : 0;
  if (actor.type === 'pc') {
    if (!basestep && !inputs.steps) {
      ui.notifications.info(game.i18n.localize('earthdawn.a.attributeNotAssigned'));
    }
  } else if (!basestep && !inputs.finalstep && !steps) {
    ui.notifications.info(game.i18n.localize('earthdawn.a.attributeNotAssigned'));
  }

  //#######################################################################################
  /** Inputs */
  //#######################################################################################
  inputs = {
    talent: title,
    actorId: actor.id,
    type: type,
    modifier: modifier,
    attribute: att,
    basestep: basestep,
    ranks: ranks,
    karmarequested: karmarequested,
    devotionrequested: devotionrequested,
    devotionDie: devotionDie,
    strain: strain,
    difficulty: difficulty,
    weaponId: damageId,
    rolltype: rolltype,
    spellName: spellName,
    spellId: spellId,
    steps: steps,
    title: title,
    initiative: initiative,
    damageBonus: damageBonus,
    targets: targets,
    defenseTarget: defenseTarget,
    defenseGroup: defenseGroup,
  };

  //#######################################################################################
  /** Rolltype  */
  //#######################################################################################
  if (rolltype === 'attack' && weapon) {
    //console.log(weapon.system.weapontype);
    inputs.attackType =
      weapon.system.weapontype === 'Melee' || weapon.system.weapontype === 'Unarmed'
        ? 'closeAttack'
        : weapon.system.weapontype === 'Ranged' || weapon.system.weapontype === 'Thrown'
        ? 'rangedAttack'
        : null;
  }

  //#######################################################################################
  /** Ammo Tracking */
  //#######################################################################################
  if (game.settings.get('earthdawn4e', 'trackAmmo')) {
    let ammoType;
    if (inputs.attackType === 'rangedAttack' && weapon.system.weapontype === 'Ranged') {
      inputs.ammoDecrement = true;
      let weaponName = weapon.name.toLowerCase();
      //console.log(weaponName);
      if (weaponName.includes(game.i18n.localize('earthdawn.c.crossbow'))) {
        ammoType = 'b.bolt';
      } else if (weaponName.includes(game.i18n.localize('earthdawn.b.bow'))) {
        ammoType = 'a.arrow';
      } else if (weaponName.includes(game.i18n.localize('earthdawn.s.sling'))) {
        ammoType = 's.stone';
      } else if (weaponName.includes(game.i18n.localize('earthdawn.b.blowgun'))) {
        ammoType = 'n.needle';
      }
      let ammoName = game.i18n.localize(`earthdawn.${ammoType}`);
      let lowerAmmoName = ammoName.toLowerCase();

      let ammoId = actor.items.filter(function (item) {
        let lowerName = item.name.toLowerCase();
        return lowerName.includes(game.i18n.localize(lowerAmmoName));
      });
      //console.log(ammoId);
      if (ammoId.length === 0 || Number(ammoId[0].system.amount) < 1) {
        ui.notifications.error('No Ammo!');
        return;
      }
      inputs.ammoId = ammoId[0]._id;
      //console.log(ammoId[0]._id);

      inputs.ammoDecrement = true;
    } else {
      inputs.ammoDecrement = false;
    }
  }

  //#######################################################################################
  /** Karma Allowed */
  //#######################################################################################

  if (karma > 0) {
    inputs.karma = karma;
  } else {
    inputs.karma = karmaAllowed(inputs);
  }

  //#######################################################################################
  /** Devotion Allowed */
  //#######################################################################################
  if (devotion > 0) {
    inputs.devotion = devotion;
  } else {
    inputs.devotion = devotionAllowed(inputs);
  }

  //#######################################################################################
  /** Active Mods */
  //#######################################################################################
  let activeMods = [];
  activeMods = await readMods(actor, inputs, activeMods);
  inputs.activeMods = activeMods;

  //#######################################################################################
  /** Option Box */
  //#######################################################################################
  let miscmod = await getOptionBox(inputs, activeMods);
  if (miscmod.cancel) {
    miscmod = {};
    return;
  }

  //#######################################################################################
  /** Active Mods Update for Step Modifier*/
  //#######################################################################################
  activeMods = [];
  inputs.stepModifier = miscmod.modifier;
  activeMods = await readMods(actor, inputs, activeMods);
  inputs.activeMods = activeMods;

  //#######################################################################################
  /** Mods */
  //#######################################################################################
  let mods = actionTestModifiers(
    actor,
    inputs.rolltype,
    rolltype === 'attack' && weapon && (weapon.system.weapontype === 'Melee' || weapon.system.weapontype === 'Unarmed') ? true : false,
    inputs.attackType,
  );
  inputs.karma = miscmod.karma;
  inputs.devotion = miscmod.devotion;
  inputs.devotionDie = miscmod.devotionDie;
  inputs.difficulty = miscmod.difficulty;
  inputs.modifier = miscmod.modifier;
  inputs.strain = miscmod.strain;
  //console.log(actor.system.bonuses.allRollsStep);
  console.log('RollType: ' + rolltype);
  //console.log("item.Type: " + item.type);
  // if (rolltype === "attack" || item){
  //   if (item.type === "talent" || item.type === "skill" || item.type === "devotion") {
  //     inputs.finalstep =
  //       Number(basestep) + Number(inputs.ranks) + Number(miscmod.modifier) - Number(wounds) + Number(mods.modifiers) + actor.system.bonuses.allRollsStep;
  //   } else if (inputs.attribute) {
  //     inputs.finalstep =
  //       Number(basestep) + Number(inputs.ranks) + Number(miscmod.modifier) - Number(wounds) + Number(mods.modifiers) + actor.system.bonuses.allRollsStep;
  //   }
  //  } else

  if (steps !== undefined && steps !== null) {
    console.log('Using Steps');
    inputs.finalstep =
      Number(steps) + Number(miscmod.modifier) - Number(wounds) + Number(mods.modifiers) + actor.system.bonuses.allRollsStep;
    //console.log(inputs.finalstep);
  } else {
    inputs.finalstep =
      Number(basestep) +
      Number(inputs.ranks) +
      Number(miscmod.modifier) -
      Number(wounds) +
      Number(mods.modifiers) +
      actor.system.bonuses.allRollsStep;
  }

  //#######################################################################################
  /** minimum step of on */
  //No Step roll can be less than 1
  //#######################################################################################
  //console.log(inputs.finalstep);
  inputs.finalstep = Number(inputs.finalstep) > 0 ? Number(inputs.finalstep) : 1;
  //console.log('Final Step is ' + inputs.finalstep);

  console.log('[EARTHDAWN] Roll Prep Outputs', inputs);
  return await rollDice(actor, inputs);
}
